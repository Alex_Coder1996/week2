function initMap() {

    var geocoder        = new google.maps.Geocoder()
    var infowindow      = new google.maps.InfoWindow();
    var locationsStores = Array.from(locationsObj);

    if ( jQuery("#da_main_map").length ) {

        const map = new google.maps.Map(document.getElementById("da_main_map"), {
            zoom: 14,
            center: {lat: -28.024, lng: 140.887},
        });

        for (i = 0; i < locationsStores.length; i++) {
            let address   = locationsStores[i][0]['store_location'][0];
            let storeName = locationsStores[i][0]['store_name'][0];
            let storeDesc = locationsStores[i][0]['store_desc'][0];
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == 'OK') {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: storeName
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent(`<h3>${storeName}</h3><p>${storeDesc}</p><p>${address}</p>`);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));

                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });

        }

    } else if ( jQuery("#da_single_map").length ) {

        const mapSingle = new google.maps.Map(document.getElementById("da_single_map"), {
            zoom: 14,
            center: {lat: -28.024, lng: 140.887},
        });

        var storeId = document.getElementById("da_single_map").getAttribute("data-post_id");

        for (i = 0; i < locationsStores.length; i++) {
            if (locationsStores[i][1] == storeId) {
                let address   = locationsStores[i][0]['store_location'][0];
                let storeName = locationsStores[i][0]['store_name'][0];
                let storeDesc = locationsStores[i][0]['store_desc'][0];

                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == 'OK') {
                        mapSingle.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: mapSingle,
                            position: results[0].geometry.location,
                            title: storeName
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                infowindow.setContent(`<h3>${storeName}</h3><p>${storeDesc}</p><p>${address}</p>`);
                                infowindow.open(mapSingle, marker);
                            }
                        })(marker, i));

                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
        }

    }

};

