<?php

/* Add a shortcodes for DA Google Maps plugin */

function da_google_maps_shortcode( $atts ) {

	return '<div id="da_main_map"></div>';

}
add_shortcode('da-google-maps', 'da_google_maps_shortcode');

function da_google_maps_single_shortcode( $atts ) {

	return '<div id="da_single_map" data-post_id="'. $atts['store_id'] . '"></div>';

}
add_shortcode('da-google-maps-single', 'da_google_maps_single_shortcode');