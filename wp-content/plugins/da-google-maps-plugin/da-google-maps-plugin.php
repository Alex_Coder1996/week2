<?php
/**
 * Plugin Name: DA Google Maps plugin
 * Description: Google Maps plugin for web4pro
 * Author:      Dmitry Alekseev
 */

require_once 'da-google-maps-shortcodes.php';

function da_add_scripts() {
	wp_enqueue_script( 'jquery' );
	wp_register_script( 'daMap', plugins_url('js/daMap.js', __FILE__), array( 'jquery' ), time(), true );
	wp_enqueue_script('daMap');
	wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD5h_5GuGfN71FPy579NTvbZhdC1YWIfQ8&callback=initMap', array( 'jquery', 'daMap' ), '', true);

	wp_localize_script('daMap', 'locationsObj', da_get_stores_info() );

	wp_enqueue_style( 'custom-css', plugins_url('css/custom.css', __FILE__) );
}
add_action('wp_enqueue_scripts', 'da_add_scripts');

function da_create_stores_post_type() {
	register_post_type(
		'stores',
		array(
			'labels'      => array(
				'name'          => __( 'Stores', 'dagmp_domain' ),
				'singular_name' => __( 'Store', 'dagmp_domain' ),
			),
			'public'      => true,
			'has_archive' => true,
			'supports'    => array( 'title', 'editor', 'custom-fields' ),
			'rewrite'     => array( 'slug' => 'stores' ),
		)
	);
}
add_action( 'init', 'da_create_stores_post_type' );

function da_get_stores_info() {

	$all_stores = get_posts(
		array(
			'post_type'      => 'stores',
			'posts_per_page' => - 1,
		)
	);

	$data = array();
	foreach ( $all_stores as $store ) {
		$data[] = array(get_post_meta( $store->ID ), $store->ID);
	}

	return $data;

}
add_action( 'init', 'da_get_stores_info' );

add_action( 'add_meta_boxes', 'da_stores_custom_fields', 1 );

function da_stores_custom_fields() {
	add_meta_box( 'da_stores_custom_fields', 'Store settings', 'da_stores_custom_fields_box_func', 'stores', 'normal', 'high' );
}

function da_stores_custom_fields_box_func( $post ) {

	$da_store_name     = get_post_meta( $post->ID, 'store_name', 1 );
	$da_store_desc     = get_post_meta( $post->ID, 'store_desc', 1 );
	$da_store_location = get_post_meta( $post->ID, 'store_location', 1 );

	wp_nonce_field('da_nonce_field', '_da_wp_nonce');
	echo '
		<p>
			<label for="store_name_id">' . __( 'Store name:' , 'dagmp_domain' ) .
				'<input type="text" id="store_name_id" name="extra[store_name]" value="' . $da_store_name . '">
			</label>
		</p>
		<p> 
			<label for="store_desc_id">' . __( 'Store description:' , 'dagmp_domain' ) .
				'<input type="text" id="store_desc_id" name="extra[store_desc]" value="' . $da_store_desc . '">
			</label>		
		</p>
		<p>
			<label for="store_location_id">' . __( 'Store location:' , 'dagmp_domain' ) .
				'<input type="text" id="store_location_id" name="extra[store_location]" value="' . $da_store_location . '">
			</label>
		</p>
	';
}

add_action( 'save_post', 'da_stores_custom_fields_update', 0 );

function da_stores_custom_fields_update( $post_id ) {

	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
		return $post_id;
	}

	if ( empty($_POST) || ! wp_verify_nonce( $_POST['_da_wp_nonce'], 'da_nonce_field' ) ){
		return $post_id;
	}

	if (
		empty( $_POST['extra'] )
		|| wp_is_post_autosave( $post_id )
		|| wp_is_post_revision( $post_id )
	) {
		return false;
	}

	$_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] );
	foreach ( $_POST['extra'] as $key => $value ) {
		if ( empty( $value ) ) {
			delete_post_meta( $post_id, $key );
			continue;
		}

		update_post_meta( $post_id, $key, $value );
	}

	return $post_id;
}