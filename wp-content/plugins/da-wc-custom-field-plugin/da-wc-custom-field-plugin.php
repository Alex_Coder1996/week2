<?php
/**
 * Plugin Name: DA wc custom field plugin
 * Description: WC custom field in checkout plugin for web4pro
 * Author:      Dmitry Alekseev
 */

add_action( 'woocommerce_after_order_notes', 'da_custom_checkout_field' );

function da_custom_checkout_field( $checkout ) {

	echo '<div id="da_custom_checkout_field"><h2>' . __( 'My field', 'dawccfp_domain' ) . '</h2>';

	woocommerce_form_field( 'my_field', array(
		'type'        => 'text',
		'class'       => array( 'my-field-class form-row-wide' ),
		'label'       => __( 'some additional info', 'dawccfp_domain' ),
		'placeholder' => __( 'Enter additional info', 'dawccfp_domain' ),
	), $checkout->get_value( 'my_field' ) );

	echo '</div>';
}

add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['my_field'] ) ) {
        update_post_meta( $order_id, 'my_field', sanitize_text_field( $_POST['my_field'] ) );
    }
}

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'da_custom_field_display_admin_order_meta', 10, 1 );

function da_custom_field_display_admin_order_meta($order){
	echo '<p><strong>'.__( 'My field', 'dawccfp_domain' ).':</strong> ' . get_post_meta( $order->get_id(), 'my_field', true ) . '</p>';
}

function da_email_order_meta( $order, $sent_to_admin, $plain_text ) {
	$my_field = get_post_meta( $order->get_id(), 'my_field', true );
	if( $my_field) {
		echo '<p><strong>' . __( 'My field', 'dawccfp_domain' ) . ': </strong> '.$my_field.'</p>';
	}

	if ( $sent_to_admin ) {
		if( $my_field) {
			echo '<p><strong>' . __( 'My field', 'dawccfp_domain' ) . '</strong> '.$my_field.'</p>';
		}
	}
}
add_action('woocommerce_email_order_meta', 'da_email_order_meta', 20, 3 );

add_action( 'woocommerce_thankyou', 'da_view_order_and_thankyou_page', 20 );
add_action( 'woocommerce_view_order', 'da_view_order_and_thankyou_page', 20 );

function da_view_order_and_thankyou_page( $order_id ){
	?>
	<table class="woocommerce-table shop_table gift_info">
		<tbody>
		<tr>
			<th><?php __( 'My field', 'dawccfp_domain' );?></th>
			<td><?php echo get_post_meta( $order_id, 'my_field', true ); ?></td>
		</tr>
		</tbody>
	</table>
	<?php
}




