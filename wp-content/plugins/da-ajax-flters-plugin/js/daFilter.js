jQuery( function( $ ){
    $( '#da_filter_post_form_id' ).on('input', function(){
        var filter = $(this);
        console.log(filter.serialize());
        $.ajax({
            url : true_obj.ajaxurl,
            action: 'da-filter',
            data : filter.serialize() + "&action=" + "da-filter",
            type : 'POST',
            beforeSend : function( xhr ) {},
            success : function( data ){
                $('.article-elem').remove();
                $( '.widget_da_ajax_filters_widget' ).append(data);
            }
        });
        return false;
    });
});