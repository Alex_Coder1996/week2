<?php
/**
 * Plugin Name: DA Ajax Filters Plugin
 * Description: Ajax filters plugin for web4pro
 * Author:      Dmitry Alekseev
 */

add_action( 'wp_enqueue_scripts', 'da_register_scripts' );

function da_register_scripts() {

	wp_enqueue_script( 'jquery' );

	wp_register_script( 'da-filter', plugins_url('js/daFilter.js', __FILE__), array( 'jquery' ), time(), true );
	wp_enqueue_script( 'da-filter' );

	wp_localize_script( 'da-filter', 'true_obj', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

function register_da_ajax_filters_widget() {
	register_widget( 'Da_ajax_filters_widget' );
}

add_action( 'wp_ajax_da-filter', 'da_filter_function' );
add_action( 'wp_ajax_nopriv_da-filter', 'da_filter_function' );

function da_filter_function(){

	$args = array(
		'posts_per_page' => $_POST['da_post_count'],
		'post_type'      => 'post',
		's'              => $_POST['da_title'],
		'orderby'        => 'post_date',
		'order'          => 'ASC',
	);

	if( isset( $_POST['da_from_date'] ) ) {
		$args[ 'date_query' ] =
			array(
				'after'     => $_POST['da_from_date'],
                'inclusive' => true,
			);
	}

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) : $query->the_post();
			?>
            <div class="article-elem">
                <a href="<?php the_permalink(); ?>"><?php echo esc_html( get_the_title() ); ?></a>
                <p>
                    Post date: <?php echo get_the_date( 'j F Y в H:i'); ?>
                </p>
            </div>
			<?php
		endwhile;
	}

	die();
}

function da_search_by_title_only( $search, $wp_query )
{
	if ( ! empty( $search ) && ! empty( $wp_query->query_vars['search_terms'] ) ) {
		global $wpdb;

		$q = $wp_query->query_vars;
		$n = ! empty( $q['exact'] ) ? '' : '%';

		$search = array();

		foreach ( ( array ) $q['search_terms'] as $term )
			$search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $term ) . $n );

		if ( ! is_user_logged_in() )
			$search[] = "$wpdb->posts.post_password = ''";

		$search = ' AND ' . implode( ' AND ', $search );
	}

	return $search;
}
add_filter( 'posts_search', 'da_search_by_title_only', 10, 2 );

add_action( 'widgets_init', 'register_da_ajax_filters_widget' );

require_once 'class-da-ajax-filters-widget.php';