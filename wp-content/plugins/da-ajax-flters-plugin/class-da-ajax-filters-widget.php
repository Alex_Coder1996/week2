<?php

class Da_Ajax_Filters_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'Da_Ajax_Filters_Widget',
			__( 'Da Ajax filters widget', 'dafp_domain' ),
			array( 'description' => __( 'Ajax filters widget from Dmitry Alekseev', 'dafp_domain' ) )
		);
	}
	public function widget( $args, $instance ) {
		$title         = apply_filters( 'widget_title', $instance['title'] );
		$da_post_count = $instance['da_post_count'];
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$filtered_posts = get_posts(
			array(
				'posts_per_page' => $da_post_count,
				'post_type'      => 'post',
				'orderby'        => 'post_date',
				'order'          => 'ASC',
			),
		);

            ?>
                <form method="POST" id="da_filter_post_form_id" name="da_filter_post_form_name" action="">
                    <p><?php echo __( 'Search', 'dafp_domain') . '/' . __('filter posts', 'dafp_domain' );?></p>
                    <label for="da_title_id"><?php _e( 'Title', 'dafp_domain' );?>:
                        <input id="da_title_id" type="text" name="da_title">
                    </label>
                    <label for="da_from_date_id"><?php _e( 'From date', 'dafp_domain' );?>:
                        <input id="da_from_date_id" type="date" name="da_from_date">
                    </label>
                    <input type="hidden" name="da_post_count" value="<?php echo $da_post_count?>">
                </form>
            <?php

		foreach ( $filtered_posts as $post ) {
			setup_postdata( $post );
            ?>
			<div class="article-elem">
				<a href="<?php the_permalink( $post->ID ); ?>"><?php echo esc_html( get_the_title( $post->ID ) ); ?></a>
				<p>
                    <?php echo __( 'Post title:', 'dafp_domain' ) . get_the_title( $post->ID ); ?>
				</p>
				<p>
                    <?php echo __( 'Post date:', 'dafp_domain' ) . get_the_date( 'j F Y в H:i', $post->ID ); ?>
				</p>
			</div>
			<?php
		}
		wp_reset_postdata();
		echo $args['after_widget'];
	}
	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Da Ajax filters widget', 'dafp_domain' );
		}

		if ( isset( $instance['da_post_count'] ) ) {
			$da_post_count = $instance['da_post_count'];
		} else {
			// default da_post_count number is 1
			$da_post_count = 1;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'dafp_domain' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'da_post_count' ); ?>"><?php _e( 'Posts count:', 'dafp_domain' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'da_post_count' ); ?>" name="<?php echo $this->get_field_name( 'da_post_count' ); ?>" type="text" value="<?php echo esc_attr( $da_post_count ); ?>" />
		</p>
		<?php
	}
	public function update( $new_instance, $old_instance ) {
		$instance                  = array();
		$instance['title']         = ( ! empty( $new_instance['title'] ) ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['da_post_count'] = ( ! empty( $new_instance['da_post_count'] ) ) ? wp_strip_all_tags( $new_instance['da_post_count'] ) : '';
		return $instance;
	}

}